#include "FarmPuzzle.h"

#if !defined(NDEBUG) && defined(WIN32)
    #include <Windows.h>
#endif
#include <assert.h>
#include <cmath>
#include "Utils\RandomUtil.h"

float const FarmPuzzle::TileProbability[] = {31.0f, 63.0f, 79.0f, 100.0f, 107.0f};  // {31.0, 32.0, 16.0, 21.0, 7.0}
float const FarmPuzzle::TilePoints[] = {0.0f, 1.0f, 5.0f, 3.0f, 10.0f};

FarmPuzzle::FarmPuzzle() :
    rowCount(6),
    columnCount(6),
    points(0.0f),
    tiles(columnCount, std::vector<FarmTileTypes>(rowCount))
{
    for (int i = 0; i < columnCount; ++i) {
        for (int j = 0; j < rowCount; ++j) {
            tiles[i][j] = pickTileType();
        }
    }
}

FarmPuzzle::~FarmPuzzle()
{
}

bool FarmPuzzle::isConnection(int column0, int row0, int column1, int row1) const {
    //return isConnection(column0 * rowCount + row0, column1 * rowCount + row1);
    assert(0 <= column0 && column0 < columnCount);
    assert(0 <= column1 && column1 < columnCount);
    assert(0 <= row0 && row0 < rowCount);
    assert(0 <= row1 && row1 < rowCount);
    // Validate type
    FarmTileTypes const type0 = tiles[column0][row0];
    FarmTileTypes const type1 = tiles[column1][row1];
    if (type0 != type1) return false;
    // Validate location
    if (std::abs(column0 - column1) <= 1 && std::abs(row0 - row1) <= 1) {
        return true;
    }
    return false;
}

std::array<std::array<bool, 3>, 3> FarmPuzzle::getConnections(int const column, int const row) const {
    assert(0 <= column && column < columnCount);
    assert(0 <= row && row < rowCount);
    FarmTileTypes const tileType = tiles[column][row];

    std::array<std::array<bool,3>, 3> result;
    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            int const columnIt = column + i;
            int const rowIt = row + j;
            if (columnIt >=0 && columnIt < columnCount
                && rowIt >= 0 && rowIt < rowCount
                && tileType == tiles[columnIt][rowIt]) {
                result[i + 1][j + 1] = true;
            } else {
                result[i + 1][j + 1] = false;
            }
        }
    }
    return result;
}

std::vector<std::vector<bool>> FarmPuzzle::getSimilarTiles(int column, int row) const {
    assert(0 <= column && column < columnCount);
    assert(0 <= row && row < rowCount);
    FarmTileTypes const tileType = tiles[column][row];

    std::vector<std::vector<bool>> result(columnCount, std::vector<bool>(rowCount, false));
    for (int i = 0; i < columnCount; ++i) {
        for (int j = 0; j < rowCount; ++j) {
            if (tiles[i][j] == tileType) result[i][j] = true;
        }

    }

    return result;
}

void FarmPuzzle::removeTilesAtPath(std::vector<std::pair<int, int>> const path) {
    if (path.size() < 3) return;
#ifndef NDEBUG
    // Validate path
    FarmTileTypes const testType = tiles[path[0].first][path[0].second];
    for (unsigned int i = 0; i < path.size() - 1; ++i) {
        assert(isConnection(path[i].first, path[i].second, path[i + 1].first, path[i + 1].second));
    }
#endif
    // Remove tiles traversed with path
    std::vector<std::pair<int, int>>::const_iterator it = path.begin();
    while (it != path.end()) {
        int const column = it->first;
        int const row = it->second;
        // Add points
        FarmTileTypes const tileType = tiles[column][row];
        points += TilePoints[tileType];
        // Remove tile
        tiles[column][row] = FarmTileTypes::None;
        ++it;
    }
    
    for (int column = 0; column < columnCount; ++column) {
        int removedCount = 0;
        // Remove empty elements...
        for (int row = rowCount - 1; row >= 0; --row) {
            if (tiles[column][row] == FarmTileTypes::None) {
                tiles[column].erase(tiles[column].begin() + row);
                ++removedCount;
            }
        }
        // ... and fill empty spaces with new ones
        for (int row = 0; row < removedCount; ++row) {
            tiles[column].insert(tiles[column].begin(), pickTileType());
        }
    }
#ifndef NDEBUG
    // Validate state
    for (int column = 0; column < columnCount; ++column) {
        for (int row = 0; row < rowCount; ++row) {
            assert(tiles[column][row] != -1);
        }
    }
#endif
}

float FarmPuzzle::getPoints() const {
    return points;
}

std::vector<std::vector<FarmTileTypes>> FarmPuzzle::getTiles() const {
    return tiles;
}

FarmTileTypes FarmPuzzle::pickTileType() {
    double const rand = RandomUtil::generate(0.0, TileProbability[FarmTileTypesCount-1]);
    for (int i = 0; i < FarmTileTypesCount; i++) {
        if (rand <= TileProbability[i]) {
            return FarmTileTypes(i);
        }
    }
    assert(false);
    return FarmTileTypes(FarmTileTypesCount - 1);
}

#ifndef NDEBUG
void FarmPuzzle::print() const {
    OutputDebugStringA("FarmPuzzle::print\n");
    char * const result = new char[rowCount*(columnCount + 1) + 1];
    char * resultPtr = result;

    for (int row = 0; row < rowCount; ++row) {
        for (int column = 0; column < columnCount; ++column) {
            *resultPtr = tiles[column][row] + 48;
            resultPtr++;
        }
        *resultPtr = '\n';
        resultPtr++;
    }
    *resultPtr = 0;

#ifdef WIN32
    OutputDebugStringA(result);
#else
    printf(result);
#endif

    delete[] result;
}

void FarmPuzzle::printSimilar(int const column, int const row) const {
    OutputDebugStringA("FarmPuzzle::printSimilar\n");
    std::vector<std::vector<bool>> const similar = getSimilarTiles(column, row);
    char * const result = new char[rowCount*(columnCount + 1) + 1];
    char * resultPtr = result;

    for (int row = 0; row < rowCount; ++row) {
        for (int column = 0; column < columnCount; ++column) {
            *resultPtr = similar[column][row] == true ? '#' : '-';
            resultPtr++;
        }
        *resultPtr = '\n';
        resultPtr++;
    }
    *resultPtr = 0;

#ifdef WIN32
    OutputDebugStringA(result);
#else
    printf(result);
#endif

    delete[] result;
}

void FarmPuzzle::printConnections(const int column, const int row) const {
    OutputDebugStringA("FarmPuzzle::printConnections\n");
    std::array<std::array<bool, 3>, 3> connections = getConnections(column, row);
    char result[9 + 3 + 1];
    char * resultPtr = result;

    for (int row = 0; row < 3; ++row) {
        for (int column = 0; column < 3; ++column) {
            *resultPtr = connections[column][row] + 48;
            resultPtr++;
        }
        *resultPtr = '\n';
        resultPtr++;
    }
    *resultPtr = 0;

#ifdef WIN32
    OutputDebugStringA(result);
#else
    printf(result);
#endif
}
#endif