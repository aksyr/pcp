#ifndef __FARMPUZZLE_H__
#define __FARMPUZZLE_H__

#include <vector>
#include <array>
#include "FarmTileTypes.h"

class FarmPuzzle
{
public:
    static float const TileProbability[];
    static float const TilePoints[];

    int const rowCount;
    int const columnCount;

private:
    float points;
    std::vector<std::vector<FarmTileTypes>> tiles; // Uses column-major order.
                                               // NEEDS to be declared after rowCount and columnCount.

public:
    FarmPuzzle();
    ~FarmPuzzle();

    bool                               isConnection(int const column0, int const row0, int const column1, int const row1) const;
    std::array<std::array<bool, 3>, 3> getConnections(int const column, int const row) const;
    std::vector<std::vector<bool>>     getSimilarTiles(int const column, int const row) const;

    void                               removeTilesAtPath(std::vector<std::pair<int, int>> const path);


    float                                   getPoints() const;
    std::vector<std::vector<FarmTileTypes>> getTiles() const;
    int                                     getRowCount() const { return rowCount; }
    int                                     getColumnCount() const { return columnCount; }
    FarmTileTypes                           at(int const column, int const row) const { return tiles[column][row]; }

#ifndef NDEBUG
    void print() const;
    void printSimilar(const int column, const int row) const;
    void printConnections(const int column, const int row) const;
#endif

private:
    static FarmTileTypes pickTileType();
};

#endif // __FARMPUZZLE_H__