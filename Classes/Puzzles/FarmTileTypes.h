#ifndef __FARMTILETYPES_H__
#define __FARMTILETYPES_H__

enum FarmTileTypes : short
{
    None = -1,
    Grass = 0,
    Tree,
    Chicken,
    Grain,
    Carrot,
    FarmTileTypesCount
};

#endif // __FARMTILETYPES_H__