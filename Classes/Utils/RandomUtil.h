#ifndef __RANDOMUTIL_H__
#define __RANDOMUTIL_H__

#include <time.h>

class RandomUtil
{
private:
    /*std::chrono::high_resolution_clock::time_point seed;
    std::default_random_engine engine;
    std::uniform_real_distribution<double> distribution;*/
    time_t seed;

public:
    RandomUtil();

    static double generate(double min, double max);
};

#endif // __RANDOMUTIL_H__