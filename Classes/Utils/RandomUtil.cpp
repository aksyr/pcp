#include "RandomUtil.h"

#include <random>

static RandomUtil instance;

RandomUtil::RandomUtil() {
    // TODO: something better than now() would be nice
    /*seed = std::chrono::high_resolution_clock::now();
    engine = std::default_random_engine(seed);*/

    seed = time(NULL);
    srand(seed);
}

double RandomUtil::generate(double min, double max) {
    return (double(rand()) / double(RAND_MAX)) * max + min;
}