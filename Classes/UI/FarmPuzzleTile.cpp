#include "FarmPuzzleTile.h"

#include "Puzzles\FarmPuzzle.h"

//enum TileTypes { Grass = 0, Tree, Chicken, Grain, Carrot, TileTypesCount };
char * const FarmPuzzleTile::FileNames[] = {
    "puzzlecraft/grass.png",
    "puzzlecraft/tree.png",
    "puzzlecraft/chicken.png",
    "puzzlecraft/grain.png",
    "puzzlecraft/carrot.png" };

USING_NS_CC;

FarmPuzzleTile::FarmPuzzleTile() :
    CCSprite(),
    faded(false),
    selected(false)
{
}

FarmPuzzleTile::~FarmPuzzleTile()
{
}

void FarmPuzzleTile::setFaded(bool const _faded) {
    if (faded == _faded) return;
    faded = _faded;
    setZOrder(faded ? -2 : 0);
}

void FarmPuzzleTile::setSelected(bool const _selected) {
    if (selected == _selected) return;
    selected = _selected;
    CCScaleTo *scaleTo = CCScaleTo::create(0.2f, selected ? 0.8f : 1.0f);
    runAction(scaleTo);
}

void FarmPuzzleTile::setTileType(FarmTileTypes type) {
    if (tileType == type) return;
    tileType = type;
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(FileNames[tileType]);
    setTexture(texture);
}

bool FarmPuzzleTile::init(FarmTileTypes type) {
    tileType = type;
    return CCSprite::initWithFile(FileNames[tileType]);
}

bool FarmPuzzleTile::init(FarmPuzzleTile const & tile) {
    FarmPuzzleTile &nTile = const_cast<FarmPuzzleTile&>(tile);
    setPosition(nTile.getPosition());
    return init(nTile.tileType);
}

FarmPuzzleTile* FarmPuzzleTile::create(FarmTileTypes type) {
    FarmPuzzleTile *pSprite = new FarmPuzzleTile();
    if (pSprite && pSprite->init(type))
    {
        pSprite->autorelease();
        return pSprite;
    }
    CC_SAFE_DELETE(pSprite);
    return NULL;
}

FarmPuzzleTile* FarmPuzzleTile::create(FarmPuzzleTile const & tile) {
    FarmPuzzleTile *pSprite = new FarmPuzzleTile();
    if (pSprite && pSprite->init(tile))
    {
        pSprite->autorelease();
        return pSprite;
    }
    CC_SAFE_DELETE(pSprite);
    return NULL;
}