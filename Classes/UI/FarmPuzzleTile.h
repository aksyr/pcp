#ifndef __FARMPUZZLETILE_H__
#define __FARMPUZZLETILE_H__

#include "cocos2d.h"
#include "Puzzles\FarmTileTypes.h"

class FarmPuzzleTile : public cocos2d::CCSprite
{
private:
    static char * const FileNames[];

    bool            faded;
    bool            selected;
    FarmTileTypes   tileType;

public:
    FarmPuzzleTile();
    virtual ~FarmPuzzleTile();

    void          setFaded(bool const faded);
    bool          isFaded() const { return faded; }
    void          setSelected(bool const selected);
    bool          isSelected() const { return selected; }
    void          setTileType(FarmTileTypes type);
    FarmTileTypes getTileType() const { return tileType; }

    virtual bool init(FarmTileTypes type);
    virtual bool init(FarmPuzzleTile const & tile);

    static FarmPuzzleTile* create(FarmTileTypes type);
    static FarmPuzzleTile* create(FarmPuzzleTile const & tile);
};

#endif // __FARMPUZZLETILE_H__