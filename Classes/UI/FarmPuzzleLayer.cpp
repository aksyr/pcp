#include "FarmPuzzleLayer.h"

#include <algorithm>
#include "Puzzles\FarmPuzzle.h"
#include "FarmPuzzleTile.h"
#include "PuzzleLine.h"

USING_NS_CC;

FarmPuzzleLayer::FarmPuzzleLayer() :
    CCLayer()
{
}

FarmPuzzleLayer::~FarmPuzzleLayer() {
    CC_SAFE_DELETE(puzzle);
}

bool FarmPuzzleLayer::init() {
    if (!CCLayer::init()) {
        return false;
    }

    // Initialize
    setTouchEnabled(true);
    puzzle = new FarmPuzzle();
    points = puzzle->getPoints();
    size = CCSize(puzzle->getColumnCount() * 100, puzzle->getRowCount() * 100 + 100);
    
    // Create tiles sprites
    tiles = std::vector<std::vector<FarmPuzzleTile*>>(puzzle->columnCount, std::vector<FarmPuzzleTile*>(puzzle->rowCount));
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        for (int row = 0; row < puzzle->getRowCount(); ++row) {
            FarmPuzzleTile *tile = FarmPuzzleTile::create(puzzle->at(column, row));
            tile->setPosition(ccp(column * 100 + 50, size.height - (row * 100) - 50));
            addChild(tile);
            tiles[column][row] = tile;
        }
    }

    // Create fading mask
    fadingLayer = CCLayerColor::create(ccc4(0, 0, 0, 0), size.width, size.height);
    fadingLayer->setZOrder(-1);
    addChild(fadingLayer);

    // Create path layer
    pathLayer = CCLayer::create();
    pathLayer->setZOrder(1);
    addChild(pathLayer);

    // Create dot sprites
    beginDot = CCSprite::create("puzzlecraft/line_dot.png");
    beginDot->setZOrder(2);
    beginDot->setVisible(false);
    addChild(beginDot);
    endDot = CCSprite::create("puzzlecraft/line_dot.png");
    endDot->setZOrder(2);
    endDot->setVisible(false);
    addChild(endDot);

    // Create points label
    pointsLabel = CCLabelTTF::create("", "arial.ttf", 60.0f);
    pointsLabel->setZOrder(3);
    pointsLabel->setHorizontalAlignment(CCTextAlignment::kCCTextAlignmentCenter);
    pointsLabel->setVerticalAlignment(CCVerticalTextAlignment::kCCVerticalTextAlignmentCenter);
    pointsLabel->setPosition(ccp(size.width/2.0f, 50.0f));
    refreshPointsLabel();
    addChild(pointsLabel);

    return true;
}

void FarmPuzzleLayer::refreshPointsLabel(bool animate) {
    char temp[32];
    sprintf(temp, "%0.0f", points);
    pointsLabel->setString(temp);
    if (animate) {
        pointsLabel->stopAllActions();
        pointsLabel->setScale(1.6f);
        pointsLabel->runAction(CCScaleTo::create(0.3f, 1.0f));
    }
}

void FarmPuzzleLayer::addPointsCallback(CCNode *sender) {
    FarmPuzzleTile *tile = static_cast<FarmPuzzleTile*>(sender);
    points += FarmPuzzle::TilePoints[tile->getTileType()];
    refreshPointsLabel(true);
    tile->removeFromParentAndCleanup(true);
}

void FarmPuzzleLayer::addPathSegment(std::pair<int, int> from, std::pair<int, int> to) {
    PuzzleLine *line;
    // Create
    if (from.first == to.first || from.second == to.second) {
        line = PuzzleLine::create();
    } else {
        line = PuzzleLine::createDiagonal();
    }
    // Rotation
    if (from.first == to.first) {
        line->setRotation(0);
    } else if (from.second == to.second) {
        line->setRotation(90);
    } else if ((from.first < to.first && from.second < to.second)
               || (to.first < from.first && to.second < from.second)) {
        line->setRotation(-45);
    } else {
        line->setRotation(45);
    }
    // Position
    CCPoint segmentPosition = ccp(
        (float(from.first) + float(to.first - from.first) / 2.0f) * 100.0f + 50.0f,
        size.height - (float(from.second) + float(to.second - from.second) / 2.0f) * 100.0f - 50.0f);
    line->setPosition(segmentPosition);
    pathLayer->addChild(line);

    refreshDotsPosition();
}

void FarmPuzzleLayer::removePathSegment() {
    pathLayer->getChildren()->removeLastObject(true);
    refreshDotsPosition();
}

void FarmPuzzleLayer::refreshDotsPosition() {
    CCPoint beginDotPosition = ccp(path.front().first * 100 + 50, size.height - path.front().second * 100 - 50);
    beginDot->setPosition(beginDotPosition);
    beginDot->setVisible(true);
    CCPoint endDotPosition = ccp(path.back().first * 100 + 50, size.height - path.back().second * 100 - 50);
    endDot->setPosition(endDotPosition);
    endDot->setVisible(true);
}

void FarmPuzzleLayer::beginPath(int const _column, int const _row) {
    std::vector<std::vector<bool>> const similarTiles = puzzle->getSimilarTiles(_column, _row);

    // Fade tiles that do not match selected
    CCFadeTo *fadeTo = CCFadeTo::create(0.3f, 160);
    fadingLayer->runAction(fadeTo);
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        for (int row = 0; row < puzzle->getRowCount(); ++row) {
            tiles[column][row]->setFaded(!similarTiles[column][row]);
        }
    }

    // Add first step
    path.push_back({_column, _row});
    tiles[_column][_row]->setSelected(true);
}

void FarmPuzzleLayer::continuePath(int const _column, int const _row) {
    if (path.size() == 0) return;
    std::pair<int, int> const previousStep = path.back();
    std::pair<int, int> const newStep(_column, _row);
    // Return if finger has not left previous tile
    if (_column == previousStep.first && _row == previousStep.second) return;

    bool const isConnection = puzzle->isConnection(previousStep.first, previousStep.second, _column, _row);
    // Check if visited previously
    std::vector<std::pair<int, int>>::iterator it = std::find(path.begin(), path.end(), newStep);
    if (isConnection && it == path.end()) {
        // Add next step
        path.push_back(newStep);
        tiles[_column][_row]->setSelected(true);
        // Add path segment
        addPathSegment(previousStep, newStep);

    } else if (it != path.end()) {
        // Revert up to two previous steps
        int const stepsToRevert = path.end() - it - 1;
        if (stepsToRevert <= 2) {
            for (int i = 0; i < stepsToRevert; ++i) {
                tiles[path.back().first][path.back().second]->setSelected(false);
                path.pop_back();
                removePathSegment();
            }
        }
    }
}

void FarmPuzzleLayer::endPath() {
    // Unfade all tiles
    CCFadeTo *fadeTo = CCFadeTo::create(0.3f, 0);
    fadingLayer->runAction(fadeTo);
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        for (int row = 0; row < puzzle->getRowCount(); ++row) {
            tiles[column][row]->setSelected(false);
        }
    }
    // Hide path
    pathLayer->removeAllChildrenWithCleanup(true);
    // Hide dots
    beginDot->setVisible(false);
    endDot->setVisible(false);
    // Clear selected elements
    removeTraversedTiles();
    // Remove steps
    path.clear();
}

void FarmPuzzleLayer::removeTraversedTiles() {
    if (path.size() < 3) return;
    // Clear drawn path
    puzzle->removeTilesAtPath(path);

    // Recover cleared tiles for later use
    std::vector<std::list<FarmPuzzleTile*>> cleared(puzzle->getColumnCount(), std::list<FarmPuzzleTile*>());
    std::vector<std::pair<int, int>>::iterator pathIt = path.begin();
    while (pathIt != path.end()) {
        cleared[pathIt->first].push_back(tiles[pathIt->first][pathIt->second]);
        ++pathIt;
    }
    // Create 'particle' effect for removed tiles
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        std::list<FarmPuzzleTile*>::iterator it = cleared[column].begin();
        while (it != cleared[column].end()) {
            FarmPuzzleTile *particle = FarmPuzzleTile::create(**it);
            addChild(particle);

            float duration = fmax(0.4f, (particle->getPosition() - pointsLabel->getPosition()).getLength() / 700.0f);
            CCEaseIn *jump = CCEaseIn::create(CCJumpTo::create(duration, pointsLabel->getPosition(), 100.0f, 1), 1.3f);
            CCCallFuncN *call = CCCallFuncN::create(this, callfuncN_selector(FarmPuzzleLayer::addPointsCallback));
            CCSequence *seq = CCSequence::create(jump, call, NULL);

            particle->runAction(seq);
            ++it;
        }
    }
    // Move cleared tiles to the front of each column
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        std::list<FarmPuzzleTile*>::iterator clearedIt = cleared[column].begin();
        int movedTiles = 0;
        while (clearedIt != cleared[column].end()) {
            // Find tile...
            std::vector<FarmPuzzleTile*>::iterator tileIt =
                std::find(tiles[column].begin(), tiles[column].end(), *clearedIt);
            FarmPuzzleTile *tile = *tileIt;
            // ... and move to beginning
            tiles[column].erase(tileIt);
            tiles[column].insert(tiles[column].begin(), tile);
            // Set position
            tile->setPosition(ccp(column * 100 + 50, size.height + (movedTiles * 100) + 50));
            ++movedTiles;
            ++clearedIt;
        }
    }
    // Refresh tile types
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        int const newTilesCount = cleared[column].size();
        int row = 0;
        for (; row < newTilesCount; ++row) {
            tiles[column][row]->setTileType(puzzle->at(column, row));
            // Animate to new position
            CCMoveTo *moveTo = CCMoveTo::create(0.3f, ccp(column * 100 + 50, size.height - (row * 100) - 50));
            tiles[column][row]->runAction(moveTo);
        }
        for (; row < puzzle->getRowCount(); ++row) {
            // Animate to new position
            CCMoveTo *moveTo = CCMoveTo::create(0.3f, ccp(column * 100 + 50, size.height - (row * 100) - 50));
            tiles[column][row]->runAction(moveTo);
        }
    }

#ifndef NDEBUG
    // Verify
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        for (int row = 0; row < puzzle->getRowCount(); ++row) {
            assert(tiles[column][row]->getTileType() == puzzle->at(column, row));
        }
    }
#endif
}

void FarmPuzzleLayer::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent) {
    CCTouch* touch = static_cast<CCTouch*>(pTouches->anyObject());
    CCPoint location = convertTouchToNodeSpace(touch);
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        for (int row = 0; row < puzzle->getRowCount(); ++row) {
            if (tiles[column][row]->boundingBox().containsPoint(location)) {
                beginPath(column, row);
                return;
            }
        }
    }
}

void FarmPuzzleLayer::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent) {
    CCTouch* touch = static_cast<CCTouch*>(pTouches->anyObject());
    CCPoint location = convertTouchToNodeSpace(touch);
    if (path.size() == 0) return;
    for (int column = 0; column < puzzle->getColumnCount(); ++column) {
        for (int row = 0; row < puzzle->getRowCount(); ++row) {
            if (tiles[column][row]->boundingBox().containsPoint(location)) {
                continuePath(column, row);
                return;
            }
        }
    }
}

void FarmPuzzleLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent) {
    CCTouch* touch = static_cast<CCTouch*>(pTouches->anyObject());
    CCPoint location = convertTouchToNodeSpace(touch);
    endPath();
}

void FarmPuzzleLayer::ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent) {
    CCLog("ccTouchesCancelled");
}