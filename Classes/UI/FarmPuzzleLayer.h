#ifndef __FARMPUZZLELAYER_H__
#define __FARMPUZZLELAYER_H__

#include <vector>
#include <list>
#include "cocos2d.h"

class FarmPuzzle;
class FarmPuzzleTile;

class FarmPuzzleLayer : public cocos2d::CCLayer
{
private:
    FarmPuzzle * puzzle;
    std::vector<std::vector<FarmPuzzleTile*>> tiles;
    std::vector<std::pair<int, int>> path;
    std::list<FarmPuzzleTile*> particles;
    float points;

    cocos2d::CCLayerColor * fadingLayer;
    cocos2d::CCLayer *      pathLayer;
    cocos2d::CCSprite *     beginDot;
    cocos2d::CCSprite *     endDot;
    cocos2d::CCLabelTTF *   pointsLabel;

    cocos2d::CCSize size;

public:
    FarmPuzzleLayer();
    virtual ~FarmPuzzleLayer();

    cocos2d::CCSize getSize() const { return size; }
    cocos2d::CCSize getScaledSize() { return size*getScale(); }

    virtual bool init();
    CREATE_FUNC(FarmPuzzleLayer);

    virtual void ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
    virtual void ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
    virtual void ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
    virtual void ccTouchesCancelled(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);

private:
    void addPointsCallback(CCNode *sender);
    void refreshPointsLabel(bool animate = false);

    void beginPath(int const column, int const row);
    void continuePath(int const _column, int const _row);
    void endPath();

    void removeTraversedTiles();

    void addPathSegment(std::pair<int, int> from, std::pair<int, int> to);
    void removePathSegment();
    void refreshDotsPosition();
};

#endif // __FARMPUZZLELAYER_H__
