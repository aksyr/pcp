#ifndef __PUZZLELINE_H__
#define __PUZZLELINE_H__

#include "cocos2d.h"

class PuzzleLine : public cocos2d::CCSprite
{
private:

public:
    PuzzleLine();
    virtual ~PuzzleLine();

    virtual bool init();
    virtual bool initDiagonal();
    static PuzzleLine* create();
    static PuzzleLine* createDiagonal();
};

#endif // __PUZZLELINE_H__