#include "PuzzleLine.h"

PuzzleLine::PuzzleLine()
{
}


PuzzleLine::~PuzzleLine()
{
}

bool PuzzleLine::init() {
    return CCSprite::initWithFile("puzzlecraft/line_short.png");
}

bool PuzzleLine::initDiagonal() {
    return CCSprite::initWithFile("puzzlecraft/line_long.png");
}

PuzzleLine* PuzzleLine::create() {
    PuzzleLine *pSprite = new PuzzleLine();
    if (pSprite && pSprite->init())
    {
        pSprite->autorelease();
        return pSprite;
    }
    CC_SAFE_DELETE(pSprite);
    return NULL;
}

PuzzleLine* PuzzleLine::createDiagonal() {
    PuzzleLine *pSprite = new PuzzleLine();
    if (pSprite && pSprite->initDiagonal())
    {
        pSprite->autorelease();
        return pSprite;
    }
    CC_SAFE_DELETE(pSprite);
    return NULL;
}