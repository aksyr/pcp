#include "FarmPuzzleScene.h"

#include "Puzzles\FarmPuzzle.h"
#include "UI\FarmPuzzleLayer.h"

USING_NS_CC;

bool FarmPuzzleScene::init() {
    CCSize const visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint const visibleOrigin = CCDirector::sharedDirector()->getVisibleOrigin();

    // Background
    CCSprite *bg = CCSprite::create("ground.png");
    addChild(bg);
    // Scale to fill
    CCSize const bgSize = bg->getContentSize();
    CCPoint const bgScale = CCPoint(visibleSize.width / bgSize.width, visibleSize.height / bgSize.height);
    float const bgScaleFactor = fmax(bgScale.x, bgScale.y);
    bg->setAnchorPoint(ccp(0.0f, 0.0f));
    bg->setScale(bgScaleFactor);

    // PuzzleLayer
    FarmPuzzleLayer *layer = FarmPuzzleLayer::create();
    addChild(layer);
    // Center and scale to fit screen (with padding)
    CCPoint const position(visibleOrigin.x + visibleSize.width / 2.0f, visibleOrigin.y + visibleSize.height / 2.0f);
 
    float const contentPadding = 0.9f;
    CCSize const layerSize = layer->getSize();
    CCPoint const scale = CCPoint(visibleSize.width*contentPadding / layerSize.width, visibleSize.height*contentPadding / layerSize.height);
    float scaleFactor = fmin(scale.x, scale.y);

    layer->setScale(scaleFactor);
    layer->setAnchorPoint(ccp(0.0f, 0.0f));
    layer->setPosition(ccp((visibleSize.width - layerSize.width*scaleFactor) / 2.0f,
        (visibleSize.height - layerSize.height*scaleFactor) / 2.0f));

    return true;
}