#ifndef __FARMPUZZLESCENE_H__
#define __FARMPUZZLESCENE_H__

#include "cocos2d.h"

class FarmPuzzleScene : public cocos2d::CCScene
{
public:
    virtual bool init();
    CREATE_FUNC(FarmPuzzleScene);
};

#endif // __FARMPUZZLESCENE_H__